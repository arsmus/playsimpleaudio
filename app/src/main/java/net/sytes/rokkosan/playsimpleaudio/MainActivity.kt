package net.sytes.rokkosan.playsimpleaudio

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.util.*

class MainActivity : AppCompatActivity() {
    val PERMISSION_READ_EX_STR: Int = 100
    private val myPlayer: MediaPlayer = MediaPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        myPlayer?.setOnCompletionListener { mp -> audioStop() }

        if (!isPermissionREAD_EXTERNAL_STORAGE()) {
            // READ_EXTERNAL_STORAGEパーミッションがない場合
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_READ_EX_STR)
        } else {
            // パーミッションを取得しているのでオーディオファイルURIを取得し再生する
            Log.d("MyApp", "playAudio")

            // disappear activity to show home screen
            var intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            this.startActivity(intent)

            playAudio(getExAudioUri())
        }
    }

    /*
     * return boolean
     *   true: permission.READ_EXTERNAL_STORAGE is granted
     *   false: permission.READ_EXTERNAL_STORAGE is not granted
     */
    fun isPermissionREAD_EXTERNAL_STORAGE(): Boolean {
        Log.d("MyApp", "In isPermissionREAD_EXTERNAL_STORAGE().")

        // アプリに権限が付与されているかどうかを確認する
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {
                Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is not granted. In isPermissionREAD_EXTERNAL_STORAGE()")
                return false
            } else {
                Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is granted.")
                return true
            }
        } else {
            Log.d("MyApp", "permission.READ_EXTERNAL_STORAGE is granted.")
            return true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permission: Array<String>,
        grantResults: IntArray) {

        Log.d("MyApp", "onRequestPermissionsResult.")

        if (grantResults.size <= 0) {
            // READ_EXTERNAL_STORAGE is not Granted
            this.finishAndRemoveTask()
        } else {
            when (requestCode) {
                PERMISSION_READ_EX_STR -> {
                    run {
                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            // 許可が取れた場合
                            // READ_EXTERNAL_STORAGE is granted
                            Log.d("MyApp", "playAudio")
                            playAudio(getExAudioUri())
                        } else {
                            // 許可が取れなかった場合
                            Toast.makeText(
                                this,
                                "パーミッションを取得できません", Toast.LENGTH_LONG
                            ).show()
                            // READ_EXTERNAL_STORAGE is not Granted
                            this.finishAndRemoveTask()
                        }
                    }
                }
            }
        }
        return
    }

    fun getExAudioUri():Uri {
        lateinit var contentUri: Uri
        val columns = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DISPLAY_NAME,
        )

        Log.d("MyApp","URI: " + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)

        val resolver = applicationContext.contentResolver
        val cursor = resolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,  //データの種類
            columns, //取得する項目 nullは全部
            null, //フィルター条件 nullはフィルタリング無し
            null, //フィルター用のパラメータ
            null   //並べ替え
        )
        Log.d( "MyApp" , Arrays.toString( cursor?.getColumnNames() ) )  //項目名の一覧を出力
        val numCount: Int? = cursor?.count
        Log.d("MyApp","Num raws : " + numCount)
        if ( numCount!! < 1 ) {
            Log.d("MyApp","No Media")
            return throw Exception("No Media")
        }
        val MediaNum = getRandomCursorNum(numCount)
        Log.d("MyApp","MediaNumRandom : " + MediaNum)

        cursor?.use {
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            val displayNameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            cursor.moveToPosition(MediaNum)
            val id = cursor.getLong(idColumn)
            val displayName = cursor.getString(displayNameColumn)
            contentUri = Uri.withAppendedPath(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                id.toString()
            )
            Log.d(
                "MyApp", "id: $id, name: $displayName, uri: $contentUri"
            )
            Toast.makeText(this, "Play: ${displayName}", Toast.LENGTH_LONG).show()
        }
        return contentUri
    }

    fun playAudio(contentUri:Uri) {
        // myPlayer.setDataSource(applicationContext, contentUri)
        Log.d("MyApp", "uri: $contentUri")
        myPlayer.setDataSource(this , contentUri)
        myPlayer.prepare()
        myPlayer.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (myPlayer != null) {
            myPlayer.reset()
            myPlayer.release()
        }
    }

    private fun audioStop(){
        myPlayer?.run {
            stop()
            reset()
            release()
            finishAndRemoveTask()
        }
    }

    fun getRandomCursorNum(maxNum: Int): Int {
        val random = Random()
        return random.nextInt(maxNum)
    }
}